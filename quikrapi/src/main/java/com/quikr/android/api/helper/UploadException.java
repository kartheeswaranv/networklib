package com.quikr.android.api.helper;

import android.net.Uri;

import com.quikr.android.network.NetworkException;
import com.quikr.android.network.Response;

import java.io.File;

/**
 * Created by Gaurav Dingolia on 27/01/16.
 */
public class UploadException extends NetworkException
{
    public Uri uri;
    public File file;
    public String path;

    public UploadException(Response response)
    {
        super(response);
    }

    public UploadException(String detailMessage)
    {
        super(detailMessage);
    }

    public UploadException(String detailMessage, Throwable throwable)
    {
        super(detailMessage, throwable);
    }
}
