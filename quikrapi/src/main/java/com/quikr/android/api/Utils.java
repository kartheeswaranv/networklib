package com.quikr.android.api;

import android.net.Uri;

import java.net.URL;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by Gaurav Dingolia on 08/01/16.
 */
class Utils
{
    private static final char[] hexCode = "0123456789abcdef".toCharArray();

    private Utils()
    {
    }

    public static String appendParams(String url, Map<String, String> params, boolean override)
    {
        try
        {
            //check if its malformed or not
            new URL(url);
            if (params == null)
                return url;

            Uri uri = Uri.parse(url);
            Uri.Builder builder = !override ? uri.buildUpon() : new Uri.Builder().scheme(uri.getScheme()).encodedAuthority
                    (uri.getAuthority()).path(uri.getPath());


            for (Iterator<Map.Entry<String, String>> iterator = params.entrySet().iterator(); iterator.hasNext(); )
            {
                Map.Entry<String, String> entry = iterator.next();
                if (!override & uri.getQueryParameter(entry.getKey()) != null)
                {
                    continue;
                }
                builder.appendQueryParameter(entry.getKey(), entry.getValue());
            }

            if(override)
            {
                Set<String> paramNames= params.keySet();
                for(String key:uri.getQueryParameterNames())
                {
                    if(!paramNames.contains(key))
                        builder.appendQueryParameter(key,uri.getQueryParameter(key));
                }
            }
            return builder.build().toString();
        }
        catch (Exception e)
        {
            throw new IllegalArgumentException("Url is malformed");
        }
    }


    public static final String generateHMAC_SHA1(String key, String message)
    {
        try
        {
            SecretKey secretKey = new SecretKeySpec(key.getBytes(), "HmacSHA1");
            Mac mac = Mac.getInstance("HmacSHA1");
            mac.init(secretKey);

            return bytesToHex(mac.doFinal(message.getBytes()));

        }
        catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
        catch (InvalidKeyException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    private static final String bytesToHex(byte[] data)
    {
        StringBuilder r = new StringBuilder(data.length * 2);
        for (byte b : data)
        {
            r.append(hexCode[(b >> 4) & 0xF]);
            r.append(hexCode[(b & 0xF)]);
        }
        return r.toString();
    }

}
