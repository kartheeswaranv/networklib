package com.quikr.android.api.helper;

import android.net.Uri;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;

/**
 * Created by Gaurav Dingolia on 27/01/16.
 */
public class UploadTask
{
    private Uri[] mUris;
    private Future[] mFutures;

    private volatile boolean mCanceled;

    UploadTask(Builder builder)
    {
        if (builder.uris.size() == 0 || builder.futures.size() == 0)
            throw new IllegalStateException("UploadTask has no element");

        mUris = builder.uris.toArray(new Uri[builder.uris.size()]);
        mFutures = builder.futures.toArray(new Future[builder.futures.size()]);
    }

    public void cancel(boolean interruptIfRunning)
    {
        if (mCanceled)
            return;

        mCanceled = true;
        cancelTasks(interruptIfRunning);
        onCanceled();
    }

    protected void onCanceled()
    {

    }

    private void cancelTasks(boolean interruptIfRunning)
    {
        for (Future future : mFutures)
        {
            if (future != null)
                future.cancel(interruptIfRunning);
        }
    }

    public boolean isCanceled()
    {
        return mCanceled;
    }

    public Uri[] getUris()
    {
        return mUris;
    }

    public Uri getUri(int index)
    {
        if (index >= mUris.length)
            throw new IllegalArgumentException("Illegal index");

        return mUris[index];
    }

    void setFuture(int index, Future future)
    {
        if (index >= mFutures.length)
            throw new ArrayIndexOutOfBoundsException("Trying to set future at illegal index");

        mFutures[index] = future;
    }

    public int getTaskCount()
    {
        return mUris.length;
    }

    static class Builder
    {
        private List<Uri> uris = new ArrayList<>();
        private List<Future> futures = new ArrayList<>();

        public Builder add(Uri uri)
        {
            addInternal(uri, null);
            return this;
        }

        public Builder add(Uri uri, Future future)
        {
            addInternal(uri, future);
            return this;
        }

        public Builder add(Uri[] uris)
        {
            return add(uris, null);
        }

        public Builder add(Uri[] uris, Future[] futures)
        {
            if (uris == null)
                throw new IllegalArgumentException("Uri cannot be null");

            if (futures != null && uris.length != futures.length)
            {
                throw new IllegalArgumentException("uri and future should be of equal length");
            }

            int index = 0;

            for (Uri uri : uris)
                addInternal(uri, futures == null ? null : futures[index++]);

            return this;
        }

        public void addInternal(Uri uri, Future future)
        {
            if (uri == null)
                throw new IllegalArgumentException("Uri cannot be null");

            uris.add(uri);
            futures.add(future);
        }

        public UploadTask build()
        {
            return new UploadTask(this);
        }

    }

}
