package com.quikr.android.api.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Gaurav Dingolia on 11/01/16.
 */
public class QDPTokenRequestBody
{
    @SerializedName("appId")
    public String appId;

    @SerializedName("signature")
    public String signature;
}
