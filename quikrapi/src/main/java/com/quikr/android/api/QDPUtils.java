package com.quikr.android.api;

/**
 * Created by Gaurav Dingolia on 08/01/16.
 */
class QDPUtils
{
    private QDPUtils(){}

    /**
     * This method is used to wrap the HMAC cryptographic hash function for QDP use. In future, this will prevent the
     * code from breaking in case we move to HMAC-SHA256.
     * @param key Secret key used to generate a signature/tag
     * @param message Message for which Message Authentication Code is required
     * @return Hex representation of HMAC signature
     */
    static final String generateHMAC(String key, String message)
    {
        return Utils.generateHMAC_SHA1(key, message);
    }



}
