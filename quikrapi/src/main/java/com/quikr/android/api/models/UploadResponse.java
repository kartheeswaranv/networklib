package com.quikr.android.api.models;

import android.net.Uri;

import com.quikr.android.network.Response;

import java.io.File;

/**
 * Created by Gaurav Dingolia on 27/01/16.
 */
public class UploadResponse<T>
{
    public Uri uri;
    public File file;
    public String path;

    public Response<T> response;
}
