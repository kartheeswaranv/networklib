package com.quikr.android.api.models;


import com.google.gson.annotations.SerializedName;

/**
 * Created by Gaurav Dingolia on 11/01/16.
 */
public class QDPTokenResponseBody
{
    @SerializedName("error")
    public String error;

    @SerializedName("message")
    public String message;

    @SerializedName("tokenId")
    public int tokenId;

    @SerializedName("token")
    public String token;

}
