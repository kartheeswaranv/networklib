package com.quikr.android.api;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Gaurav Dingolia on 11/01/16.
 */
class SharedPreferenceStorage implements Storage
{
    private SharedPreferences mPreferences;

    public SharedPreferenceStorage(Context context, String prefFileName)
    {
        if(context == null || prefFileName == null)
        {
            throw  new IllegalArgumentException("Context or Preference file cannot be null");
        }

        mPreferences  = context.getSharedPreferences(prefFileName,Context.MODE_PRIVATE);
    }

    public SharedPreferenceStorage (SharedPreferences sharedPreferences)
    {
        if(sharedPreferences == null)
        {
            throw  new IllegalArgumentException("Shared Preferences cannot be null");
        }

        mPreferences = sharedPreferences;
    }



    @Override
    public void store(String key, String value)
    {

        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString(key,value);
        editor.apply();

    }

    @Override
    public String retrieve(String key)
    {
        return mPreferences.getString(key,null);
    }

    @Override
    public void remove(String key)
    {
        mPreferences.edit().remove(key).apply();
    }
}
