package com.quikr.android.api.encryption;

import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;

import com.quikr.android.api.QuikrNetwork;
import com.quikr.android.api.QuikrRequest;
import com.quikr.android.network.Request;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.Scanner;
import java.util.Set;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import static android.R.attr.start;

/**
 * Created by jigneshshah on 03/10/16.
 */

public class EncryptionHelper {
    private static final String ENCRYPTION_ALGO_AES = "AES/CBC/PKCS5PADDING";
    private static final String UTF_8_CHARSET = "UTF-8";
    private static final String TAG = "EncryptionHelper";

    private static String encrypt(byte[] input, boolean compress) throws GeneralSecurityException
        , UnsupportedEncodingException {

        long startTime = System.currentTimeMillis();

        if (compress) {
            try (ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                 final GZIPOutputStream gzipOutputStream = new GZIPOutputStream(byteArrayOutputStream);
            ) {
                gzipOutputStream.write(input);
                gzipOutputStream.flush();
                gzipOutputStream.close();

                byte[] compressedBytes = byteArrayOutputStream.toByteArray();

                long compressEndTime = System.currentTimeMillis();
                Log.d(TAG, "compressTime: "+ (compressEndTime - startTime));
                Log.d(TAG, "compression: " + input.length + " "
                    + compressedBytes.length);

                input = compressedBytes;

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        String secret = QuikrNetwork.getQuikrContext().getQDPMetaData().getAppSecret();
        secret = secret.substring(0, secret.length() / 2);

        byte[] secretBytes = secret.getBytes(UTF_8_CHARSET);

        SecretKeySpec key = new SecretKeySpec(secretBytes, "AES");

        Cipher aesCipherForEncryption = Cipher.getInstance(ENCRYPTION_ALGO_AES);

        aesCipherForEncryption.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(secretBytes));

        byte[] encryptedBytes = aesCipherForEncryption.doFinal(input);
        long encryptedEndTime = System.currentTimeMillis();

        return Base64.encodeToString(encryptedBytes, Base64.NO_WRAP);
    }

    private static String decrypt(String input, boolean decompress)
        throws GeneralSecurityException, UnsupportedEncodingException {
        long start = System.currentTimeMillis();
        String secret = QuikrNetwork.getQuikrContext().getQDPMetaData().getAppSecret();
        secret = secret.substring(0, secret.length() / 2);

        byte[] secretBytes = secret.getBytes(UTF_8_CHARSET);

        SecretKeySpec key = new SecretKeySpec(secretBytes, "AES");

        Cipher aesCipherForEncryption = Cipher.getInstance(ENCRYPTION_ALGO_AES);

        aesCipherForEncryption.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(secretBytes));

        byte[] decodedResponse;
        try {
            decodedResponse = Base64.decode(input, Base64.NO_WRAP);
        }
        catch (IllegalArgumentException e) {
            return null;
        }
        final byte[] clearBytes = aesCipherForEncryption.doFinal(decodedResponse);

        long decryptEndTime = System.currentTimeMillis();

        if (decompress) {
            try (
                ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(clearBytes);
                final GZIPInputStream gzipInputStream = new GZIPInputStream(byteArrayInputStream);
                Scanner s = new Scanner(gzipInputStream, UTF_8_CHARSET).useDelimiter("\\A");
            ) {
                String decompressedString = s.hasNext() ? s.next() : "";
                long decompressEndTime = System.currentTimeMillis();

                Log.d(TAG, "decompressTime: "+ (decompressEndTime - decryptEndTime));
                Log.d(TAG, "decompression: " + clearBytes.length + " "
                    + decompressedString.length());
                return decompressedString;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return new String(clearBytes, UTF_8_CHARSET);
    }

    public static String getEncodedUrlForGetRequest(String url) throws GeneralSecurityException, UnsupportedEncodingException {
        String[] urlParts = url.split("\\?");
        if (urlParts.length < 2) {
            //if there are no params
            return url;
        }

        String queryParams = urlParts[1];
        String encodedQueryParams = encrypt(queryParams.getBytes(UTF_8_CHARSET), false);
        return urlParts[0] + "?" + encodedQueryParams;
    }

    public static String getEncodedRequestBody(byte[] requestBody, boolean compress)
        throws GeneralSecurityException, UnsupportedEncodingException {
        return encrypt(requestBody, compress);
    }

    public static String decodeNetworkResponse(byte[] response, boolean decompress) {
        try {
            return decrypt(new String(response, UTF_8_CHARSET), decompress);
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static Request encryptRequest(QuikrRequest.Builder builder, boolean isCompressionEnabled) {
        Request request = builder.request.build();
        switch (request.getMethod()) {
            case GET:
                String encodedUrl = null;
                try {
                    encodedUrl = getEncodedUrlForGetRequest(request.getUrl());
                } catch (GeneralSecurityException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                if (encodedUrl != null) {
                    builder.request = request.newBuilder()
                        .setUrl(encodedUrl);
                    return builder.request.build();
                } else {
                    throw new IllegalArgumentException("Error encrypting the GET request");
                }
            case PUT:
            case POST:
                if (request.getBody() == null) {
                    return builder.request.build();
                }
                String encodedBody = null;
                try {
                    encodedBody = getEncodedRequestBody(request.getBody(), isCompressionEnabled);
                } catch (GeneralSecurityException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                if (encodedBody != null) {
                    builder.request = request.newBuilder().setBody(encodedBody.getBytes());
                    if (isCompressionEnabled) {
                        builder.request.addHeader(QuikrRequest.X_QUIKR_CONTENT_ENCODING
                            , QuikrRequest.GZIP);
                    }

                    return builder.request.build();
                } else {
                    throw new IllegalArgumentException("Error encrypting the request body");
                }
        }

        return builder.request.build();
    }

    public static boolean isEncryptionRequiredForUrl(String url) {
        Set<String> supportedUrls = QuikrNetwork.getQuikrContext().getEncrytionSupportedUrls();
        try {
            URI uri = new URI(url);
            String domainName = uri.getHost();
            int port = uri.getPort();
            if (!TextUtils.isEmpty(domainName)) {
                if (port > 0) {
                    domainName = domainName + ":" + port;
                }
                return supportedUrls != null && supportedUrls.contains(domainName);
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return false;
    }
}
