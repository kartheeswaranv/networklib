package com.quikr.android.api.encryption;

import android.util.Log;

import com.quikr.android.api.QuikrRequest;
import com.quikr.android.network.converter.Converter;
import com.quikr.android.network.converter.ResponseBodyConverter;

import java.util.Collections;
import java.util.Map;

import static com.quikr.android.api.QuikrRequest.GZIP;
import static com.quikr.android.api.QuikrRequest.X_QUIKR_CONTENT_ENCODING;

/**
 * Created by jigneshshah on 03/10/16.
 */

public class DecryptResponseBodyConverter<T> extends ResponseBodyConverter<T>
    implements Converter.HeaderAwareConverter<byte[], T> {

    private ResponseBodyConverter<T> mConverter;
    protected boolean mQuikrEncodingAccepted;
    protected Map<String, String> mHeaders = Collections.emptyMap();

    public DecryptResponseBodyConverter(ResponseBodyConverter<T> converter
        , boolean quikrEncodingAccepted) {
        mConverter = converter;
        mQuikrEncodingAccepted = quikrEncodingAccepted;
    }

    @Override
    public T convert(byte[] bytes) {
        if (QuikrRequest.canDebug) {
            Log.e(QuikrRequest.TAG, "Response before decrypting = " + new String(bytes));
        }

        Object quikrContentEncoding = mHeaders.get(X_QUIKR_CONTENT_ENCODING);
        boolean decodeRequired = mQuikrEncodingAccepted && GZIP.equals(quikrContentEncoding);

        String decodedResponsee = EncryptionHelper.decodeNetworkResponse(bytes, decodeRequired);

        if (QuikrRequest.canDebug) {
            Log.e(QuikrRequest.TAG, "Response after decrypting = " + decodedResponsee);
        }

        if(decodedResponsee == null) return null;
        return mConverter.convert(decodedResponsee.getBytes());
    }

    @Override
    public void setHeaders(Map<String, String> headers) {
        mHeaders = headers;
    }
}
