package com.quikr.android.api;

import com.quikr.android.network.Request;

/**
 * Created by Gaurav Dingolia on 12/01/16.
 */
public class DefaultSubmissionPolicy implements SubmissionPolicy
{

    @Override
    public boolean isSubmissionAllowed(Request request)
    {
        return !QDPHelper.isQDPRequest(request) || QDPHelper.getInstance().isTokenValid() || !QDPHelper.getInstance()
                .isTokenProcessing();
    }
}
