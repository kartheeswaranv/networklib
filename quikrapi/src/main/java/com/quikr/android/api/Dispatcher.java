package com.quikr.android.api;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.quikr.android.network.Authenticator;
import com.quikr.android.network.Callback;
import com.quikr.android.network.NetworkException;
import com.quikr.android.network.NetworkManager;
import com.quikr.android.network.Request;
import com.quikr.android.network.RequestFilter;
import com.quikr.android.network.Response;
import com.quikr.android.network.converter.ResponseBodyConverter;

import java.util.Collection;
import java.util.Iterator;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * A {@link Dispatcher} can wrap any given {@link NetworkManager} and forward requests to it based on the
 * {@link SubmissionPolicy#isSubmissionAllowed(Request)}.
 * <p>
 * If submission is not allowed, the incoming requests are queued till client calls {@link #proceed()}. If client
 * cancel the request(s) which are queued, they will just be cleared of without actually submitting it to the wrapped
 * {@link NetworkManager}.</p>
 *
 * @author Gaurav.Dingolia
 * @see SubmissionPolicy
 * @since 1.0
 */
final class Dispatcher implements NetworkManager
{
    //TODO: Decouple synchronization related code from dispatcher

    private static final String TAG = "Dispatcher";

    private NetworkManager mNetworkManager;
    private SubmissionPolicy mSubmissionPolicy;

    private Queue<DispatchRequest> mQueuedRequests;
    private CountDownLatch mLatch;

    public Dispatcher(NetworkManager manager)
    {
        this(manager, new DefaultSubmissionPolicy());
    }

    public Dispatcher(NetworkManager manager, SubmissionPolicy policy)
    {
        mNetworkManager = manager;
        mSubmissionPolicy = policy;

        mQueuedRequests = new PriorityQueue<>();
        mLatch = new CountDownLatch(1);
    }

    @Override
    public void execute(Request request)
    {
        execute(request,null,null);
    }

    @Override
    public void executeSynchronously(Request request)
    {
        if (!mSubmissionPolicy.isSubmissionAllowed(request))
        {
            try
            {
                mLatch.await();
                mNetworkManager.executeSynchronously(request);
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        }
        else
        {
            mNetworkManager.executeSynchronously(request);
        }

    }

    @Override
    public <T> void execute(final Request request, final Callback<T> callback, final ResponseBodyConverter<T> converter)
    {
        if(Looper.myLooper() != Looper.getMainLooper()){
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    performExecutionTask(request,callback,converter);
                }
            });
        }else{
            performExecutionTask(request,callback,converter);
        }
    }

    private <T> void performExecutionTask(Request request, Callback<T> callback, ResponseBodyConverter<T> converter){
        if (mSubmissionPolicy.isSubmissionAllowed(request))
        {
            mNetworkManager.execute(request, callback, converter);
            return;
        }
        addToQueue(new DispatchRequest(request, callback, converter));
    }

    @Override
    public <T> Response<T> executeSynchronously(Request request, ResponseBodyConverter<T> converter) throws
            NetworkException
    {
        if (!mSubmissionPolicy.isSubmissionAllowed(request))
        {
            try
            {
                mLatch.await();
                return mNetworkManager.executeSynchronously(request, converter);
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        }
        else
        {
            return mNetworkManager.executeSynchronously(request, converter);
        }
        return null;
    }

    private void addToQueue(DispatchRequest request)
    {
        mQueuedRequests.offer(request);
    }

    @Override
    public void cancel(Request request)
    {
        if (!remove(request))
            mNetworkManager.cancel(request);
    }

    @Override
    public void cancelAll(Collection<? extends Request> requests)
    {
        remove(requests);
        mNetworkManager.cancelAll(requests);
    }

    @Override
    public void cancelWithTag(final Object tag)
    {
        remove(tag);
        mNetworkManager.cancel(new RequestFilter()
        {
            @Override
            public boolean apply(Request request)
            {
                Object requestTag = request.getTag();
                if (requestTag == null) return false;
                if (requestTag instanceof QuikrRequest.TagWrapper)
                {
                    requestTag = ((QuikrRequest.TagWrapper) requestTag).getActualTag();
                }
                return requestTag != null && requestTag.equals(tag);
            }
        });
    }

    @Override
    public void cancel(RequestFilter requestFilter)
    {
        remove(requestFilter);
        mNetworkManager.cancel(requestFilter);
    }


    private synchronized boolean remove(Request request)
    {
        for (Iterator<DispatchRequest> iterator = mQueuedRequests.iterator(); iterator.hasNext(); )
        {
            DispatchRequest dispatchRequest = iterator.next();
            if (dispatchRequest.request == request)
            {
                iterator.remove();
                return true;
            }
        }
        return false;
    }

    private synchronized void remove(Collection<? extends Request> requests)
    {
        for (Iterator<DispatchRequest> iterator = mQueuedRequests.iterator(); iterator.hasNext(); )
        {
            DispatchRequest dispatchRequest = iterator.next();
            if (requests.contains(dispatchRequest.request))
            {
                iterator.remove();
            }
        }
    }

    private synchronized void remove(Object actualTag)
    {
        for (Iterator<DispatchRequest> iterator = mQueuedRequests.iterator(); iterator.hasNext(); )
        {
            DispatchRequest dispatchRequest = iterator.next();
            Object tag = dispatchRequest.request.getTag();
            if (tag instanceof QuikrRequest.TagWrapper)
            {
                tag = ((QuikrRequest.TagWrapper) tag).getActualTag();
            }
            if (tag != null && tag.equals(actualTag))
            {
                iterator.remove();
            }
        }
    }

    private synchronized void remove(RequestFilter requestFilter)
    {
        if (requestFilter == null) return;
        for (Iterator<DispatchRequest> iterator = mQueuedRequests.iterator(); iterator.hasNext(); )
        {
            DispatchRequest dispatchRequest = iterator.next();
            if (requestFilter.apply(dispatchRequest.request))
            {
                iterator.remove();
            }
        }
    }

    @Override
    public void setAuthenticator(Authenticator authenticator)
    {
        mNetworkManager.setAuthenticator(authenticator);
    }

    public SubmissionPolicy getSubmissionPolicy()
    {
        return mSubmissionPolicy;
    }

    public void await(long timeout, TimeUnit unit)
    {
        try
        {
            mLatch.await(timeout, unit);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }

    public synchronized void resetLatchIfNeeded()
    {
        if (mLatch.getCount() == 0)
        {
            mLatch = new CountDownLatch(1);
        }
    }

    public void proceed()
    {
        Log.d(TAG, "Proceeding");
        mLatch.countDown();
        executeQueuedRequests();
    }

    public void executeQueuedRequests()
    {
        DispatchRequest dispatchRequest;
        while ((dispatchRequest = mQueuedRequests.poll()) != null)
        {
            QDPHelper.doQDPRelatedStuff(dispatchRequest.request);

            if (dispatchRequest.callback != null)
                execute(dispatchRequest.request, dispatchRequest.callback, dispatchRequest.converter);
            else
                execute(dispatchRequest.request);
        }
    }

    public synchronized void clearQueuedRequests()
    {
        mQueuedRequests.clear();
    }

    private class DispatchRequest<T> implements Comparable<DispatchRequest>
    {
        private Request request;
        private Callback<T> callback;
        private ResponseBodyConverter<T> converter;

        DispatchRequest(Request request, Callback<T> callback, ResponseBodyConverter<T> converter)
        {
            this.request = request;
            this.callback = callback;
            this.converter = converter;
        }

        @Override
        public int compareTo(DispatchRequest another)
        {
            return request.compareTo(another.request);
        }
    }


}
