package com.quikr.android.api;

import android.util.Log;

import com.quikr.android.api.encryption.DecryptResponseBodyConverter;
import com.quikr.android.api.encryption.EncryptionHelper;
import com.quikr.android.network.Callback;
import com.quikr.android.network.Headers;
import com.quikr.android.network.Method;
import com.quikr.android.network.NetworkException;
import com.quikr.android.network.NetworkManager;
import com.quikr.android.network.Request;
import com.quikr.android.network.Response;
import com.quikr.android.network.body.RequestBody;
import com.quikr.android.network.converter.RequestBodyConverter;
import com.quikr.android.network.converter.ResponseBodyConverter;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 *
 */
public class QuikrRequest
{
    public static final String X_QUIKR_CONTENT_ENCODING = "X-Quikr-Content-Encoding";
    public static final String GZIP = "gzip";
    protected static final String X_QUIKR_ACCEPT_ENCODING = "X-Quikr-Accept-Encoding";
    private NetworkManager mNetworkManager;
    private Request mRequest;
    private boolean mCanceled;
    private boolean mIsQDP;
    private TagWrapper mTagWrapper;
    private boolean isEncryptionEnabled;
    public final boolean isCompressionEnabled;

    public static final boolean canDebug = false;

    public static final String TAG = QuikrRequest.class.getSimpleName();

    //reference passed to the network library
    private Callback mNetworkCallback = new Callback() {
        @Override
        public void onSuccess(Response response) {
            if (mHostCallback != null) {
                mHostCallback.onSuccess(response);
            }
        }

        @Override
        public void onError(NetworkException e) {
            if (isEncryptionEnabled && e.getResponse() != null && e.getResponse().getBody() != null) {
                try {
                    Object quikrContentEncoding = e.getResponse().getHeaders()
                        .get(X_QUIKR_CONTENT_ENCODING);
                    boolean decompress = GZIP.equals(quikrContentEncoding);

                    String decodedResponse = EncryptionHelper.decodeNetworkResponse(
                        ((String) e.getResponse().getBody()).getBytes(), decompress);
                    e.getResponse().setBody(decodedResponse);
                } catch (Exception ex) {
                    //safety net
                }
            }
            if (mHostCallback != null) {
                mHostCallback.onError(e);
            }
        }
    };

    //callback reference given by the calling app
    private Callback mHostCallback;

    private QuikrRequest(QuikrRequest.Builder builder)
    {
        mNetworkManager = QuikrNetwork.getNetworkManager();
        mIsQDP  = builder.isQDP;

        builder.request.setTag(mTagWrapper = new TagWrapper(builder.mTag,this));

        Request request = builder.request.build();

        if (builder.appendBasicParams && QuikrNetwork.getQuikrContext().getBasicParams(mIsQDP) != null)
        {
            builder.request = request.newBuilder()
                    .setUrl(Utils.appendParams(request.getUrl(), QuikrNetwork.getQuikrContext().getBasicParams(mIsQDP),
                            false));
        }

        if(builder.appendBasicHeaders && QuikrNetwork.getQuikrContext().getBasicHeaders(mIsQDP) != null)
        {
            for(Iterator<Map.Entry<String,String>> iterator = QuikrNetwork.getQuikrContext().getBasicHeaders(mIsQDP)
                    .entrySet().iterator();iterator.hasNext();)
            {
                Map.Entry<String, String> entry = iterator.next();

                if(!request.getHeaders().getHeaders().containsKey(entry.getKey()))
                    builder.addHeader(entry.getKey(),entry.getValue());
            }
        }

        if (builder.appendUTMParams && QuikrNetwork.getQuikrContext().getUTMParams() != null) {
            request = builder.request.build();
            builder.request = request.newBuilder()
                    .setUrl(Utils.appendParams(request.getUrl(), QuikrNetwork.getQuikrContext().getUTMParams(),
                            true));
        }

        mRequest = builder.request.build();

        isEncryptionEnabled = QuikrNetwork.getQuikrContext().isEncryptionEnabled() && EncryptionHelper.isEncryptionRequiredForUrl(mRequest.getUrl());

        Map<String, String> params = new HashMap<>();
        params.put(QuikrContext.PARAM_URL, mRequest.getUrl());
        params.put(QuikrContext.PARAM_CONTENT_TYPE, mRequest.getContentType());
        isCompressionEnabled = QuikrNetwork.getQuikrContext().isCompressionRequiredForParams(params);

        if (isEncryptionEnabled) {
            String body = null;
            try {
                body = !mRequest.getMethod().equals(Method.GET) ? (mRequest.getBody() != null ? new String(mRequest.getBody(), "UTF-8") : "NULL") : "NONE";
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            if (canDebug) {
                Log.e(TAG, ("Url before encrypting = " + this.mRequest.getUrl()));
                Log.e(TAG, ("Request body before encrypting = " + body));
            }

            if (isCompressionEnabled) {
                builder.request.addHeader(X_QUIKR_ACCEPT_ENCODING, GZIP);
            }

            mRequest = EncryptionHelper.encryptRequest(builder, isCompressionEnabled);

            if (canDebug) {
                Log.e(TAG, ("Url after encrypting = " + this.mRequest.getUrl()));
                Log.e(TAG, ("Request body after encrypting = " + (this.mRequest.getBody() != null ? new String(this.mRequest.getBody()) : "NULL")));
            }
        } else {
            if (canDebug) {
                Log.i(TAG, "Skipping encryption for " + mRequest.getUrl());
            }
        }

        if (mIsQDP)
        {
            QDPHelper.doQDPRelatedStuff(mRequest);
        }

    }

    /**
     * Executes this request using the default {@link NetworkManager} implementation.
     */
    public void execute()
    {
        mNetworkManager.execute(mRequest);
    }

    /**
     * Executes the request using the default {@link NetworkManager} implementation and parse the response body using
     * the supplied {@link ResponseBodyConverter}.
     *
     * @param callback Callback for the request
     * @param converter Converter to be used to parse the response body
     * @param <T> Type to which body should be converted
     */
    public <T> void execute(Callback<T> callback, ResponseBodyConverter<T> converter)
    {
        mHostCallback = callback;
        mNetworkManager.execute(mRequest, mNetworkCallback, isEncryptionEnabled ?
            new DecryptResponseBodyConverter<>(converter, isCompressionEnabled) : converter);
    }

    /**
     * Cancels this request
     */
    public void cancel()
    {
        if (mCanceled)
            return;

        mNetworkManager.cancel(mRequest);
        mCanceled = true;
        mHostCallback = null;
    }

    public boolean isCancelled()
    {
        return mCanceled;
    }

    public boolean isQDPRequest()
    {
        return mIsQDP;
    }

    /**
     * Gets HTTP url for the request.
     *
     * @return url
     */
    public String getUrl()
    {
        return mRequest.getUrl();
    }

    /**
     * Gets HTTP request headers encapsulated as {@link Headers}
     *
     * @return
     */
    public Headers getHeaders()
    {
        return mRequest.getHeaders();
    }

    /**
     * Gets raw HTTP body
     *
     * @return HTTP body
     */
    public byte[] getBody()
    {
        return mRequest.getBody();
    }

    /**
     * @return
     */
    public Method getMethod()
    {
        return mRequest.getMethod();
    }

    /**
     * @return
     */
    public Request.Priority getPriority()
    {
        return mRequest.getPriority();
    }

    /**
     * @return
     */
    public Object getTag()
    {
        return mTagWrapper.mActualTag;
    }

    /**
     * Gets content type of HTTP request body.
     *
     * @return
     */
    public String getContentType()
    {
        return mRequest.getContentType();
    }


    /**
     *
     */
    public static class Builder
    {
        public Request.Builder request = new Request.Builder();
        private boolean isQDP;
        private boolean appendUTMParams = true;
        private boolean appendBasicParams;
        private boolean appendBasicHeaders;
        private Object mTag;


        public Builder setMethod(Method method)
        {
            request.setMethod(method);
            return this;
        }

        public Builder setUrl(String url)
        {
            request.setUrl(url);
            return this;
        }

        public Builder appendUTMParams(boolean appendUTMParams) {
            this.appendUTMParams = appendUTMParams;
            return this;
        }

        public Builder setHeaders(Headers headers)
        {
            request.setHeaders(headers);
            return this;
        }

        public Builder addHeader(String key, String value)
        {
            request.addHeader(key, value);
            return this;
        }

        public Builder addHeaders(Map<String,String> headers)
        {
            if(headers == null) return this;

            for(Iterator<Map.Entry<String,String>> iterator = headers.entrySet().iterator();iterator.hasNext();)
            {
                Map.Entry<String,String> entry = iterator.next();
                request.addHeader(entry.getKey(),entry.getValue());
            }

            return this;
        }

        public Builder setBody(RequestBody body)
        {
            request.setBody(body);
            return this;
        }

        public <T> Builder setBody(T body, RequestBodyConverter<T> converter)
        {
            request.setBody(body, converter);
            return this;
        }

        public Builder setBody(byte[] body)
        {
            request.setBody(body);
            return this;
        }

        //TODO: Decide whether to expose this or not
        public Builder setPriority(Request.Priority priority)
        {
            request.setPriority(priority);
            return this;
        }

        public Builder setContentType(String contentType)
        {
            request.setContentType(contentType);
            return this;
        }

        public Builder setTag(Object tag)
        {
            mTag = tag;
            return this;
        }

        public Builder setQDP(boolean isQDP)
        {
            this.isQDP = isQDP;
            return this;
        }

        public Builder appendBasicParams(boolean value)
        {
            appendBasicParams = value;
            return this;
        }

        public Builder appendBasicHeaders(boolean value)
        {
            appendBasicHeaders = value;
            return this;
        }

        public QuikrRequest build()
        {
            return new QuikrRequest(this);
        }

    }

    static class TagWrapper
    {
        private Object mActualTag;
        private Object mWrapperTag;

        private TagWrapper(Object actualTag, Object wrapperTag)
        {
            mActualTag = actualTag;
            mWrapperTag = wrapperTag;
        }

        Object getActualTag()
        {
            return  mActualTag;
        }

        Object getWrapperTag()
        {
            return mWrapperTag;
        }

    }

}
