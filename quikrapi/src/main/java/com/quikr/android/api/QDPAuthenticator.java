package com.quikr.android.api;

import android.util.Log;

import com.quikr.android.network.Authenticator;
import com.quikr.android.network.Request;
import com.quikr.android.network.Response;

import java.util.concurrent.TimeUnit;

import static com.quikr.android.api.QDPHelper.KEY_QDP_TOKEN_ADDED_TIMESTAMP;

/**
 * Created by Gaurav Dingolia on 12/01/16.
 */
class QDPAuthenticator implements Authenticator
{
    private static final String TAG = "QDPAuthenticator";

    @Override
    public Request authenticate(Request request, Response response)
    {
        Log.d(TAG, "Got 401 error for: "+request.getUrl());

        // we want to modify only QDP requests
        if( QDPHelper.isQDPRequest(request))
        {
            Long requestTokenTimestamp = (Long) request.markers.get(KEY_QDP_TOKEN_ADDED_TIMESTAMP);

            if (requestTokenTimestamp == null
                || requestTokenTimestamp >= QDPHelper.getInstance().getTokenTimestamp()) {
                Log.d(TAG, "Calling generateNewToken");
                // Will take care of preventing any new token generation request, if the request is already in flight
                QDPHelper.getInstance().generateNewToken();
            }

            //Wait for ongoing token generation, if any
            Log.d(TAG, "Calling await");
            QuikrNetwork.getDispatcher().await(QDPHelper.TOKEN_GEN_TIMEOUT, TimeUnit.SECONDS);
            Log.d(TAG, "Await over");

            //Token generation has failed and we don't want to retry
            if(!QDPHelper.getInstance().isTokenValid())
                return null;

            //If we get here, the token has refreshed. Add the new headers accordingly
            return QDPHelper.doQDPRelatedStuff(request);

        }
        //dont retry for 401 response for non QDP requests
        return null;
    }


}
