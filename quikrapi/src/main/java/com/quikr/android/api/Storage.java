package com.quikr.android.api;

/**
 * Created by Gaurav Dingolia on 11/01/16.
 */
interface Storage
{
    void store(String key, String value);

    String retrieve(String key);

    void remove(String key);
}
