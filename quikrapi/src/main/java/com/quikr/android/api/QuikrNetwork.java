package com.quikr.android.api;

import com.android.volley.RequestQueue;
import com.quikr.android.network.NetworkManager;
import com.quikr.android.network.VolleyContext;
import com.quikr.android.network.VolleyNetworkManagerImpl;

/**
 * Created by Gaurav Dingolia on 05/01/16.
 */
public class QuikrNetwork
{
    private static volatile Dispatcher mDispatcher;
    private static QuikrContext mQuikrContext;

    private QuikrNetwork()
    {
    }

    public static void initialize(QuikrContext context)
    {
        if (context == null)
        {
            throw new IllegalArgumentException("QuikrContext is null");
        }
        mQuikrContext = context;

        QDPHelper.getInstance().refreshTokenIfNeeded();

        // Do other initialization here if required. The idea of having this method is to do all the setup tasks here
        // . Also we can check if we have required permissions for library or not etc

    }

    public static QuikrContext getQuikrContext()
    {
        return mQuikrContext;
    }

    public static NetworkManager getNetworkManager()
    {
        return getDispatcher();
    }

    static Dispatcher getDispatcher()
    {
        if (mDispatcher == null)
        {
            synchronized (QuikrNetwork.class)
            {
                if (mDispatcher == null)
                {
                    if(mQuikrContext.getContextProperties()!=null && mQuikrContext.getContextProperties().get(QuikrContext.Properties.VOLLEY_CONTEXT)!=null) {
                        VolleyNetworkManagerImpl volleyNetworkManager = new VolleyNetworkManagerImpl(mQuikrContext.getApplicationContext(),(VolleyContext)mQuikrContext.getContextProperties().get(QuikrContext.Properties.VOLLEY_CONTEXT));
                        mDispatcher = new Dispatcher(volleyNetworkManager);
                    }else{
                        mDispatcher = new Dispatcher(new VolleyNetworkManagerImpl(mQuikrContext.getApplicationContext()));
                    }
                    mDispatcher.setAuthenticator(new QDPAuthenticator());
                }
            }
        }

        return mDispatcher;
    }

}
