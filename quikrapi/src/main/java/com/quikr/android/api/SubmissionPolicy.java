package com.quikr.android.api;

import com.quikr.android.network.Request;

/**
 * Created by Gaurav Dingolia on 12/01/16.
 */
public interface SubmissionPolicy
{
    boolean isSubmissionAllowed(Request request);
}
