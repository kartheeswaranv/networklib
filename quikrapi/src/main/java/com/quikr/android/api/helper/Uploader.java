package com.quikr.android.api.helper;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;

import com.quikr.android.network.HTTPRequest;
import com.quikr.android.network.Headers;
import com.quikr.android.network.Method;
import com.quikr.android.network.Response;
import com.quikr.android.network.body.FileRequestBody;
import com.quikr.android.network.body.MultiPartRequestBody;
import com.quikr.android.network.body.RequestBody;
import com.quikr.android.network.body.UriRequestBody;
import com.quikr.android.network.converter.ResponseBodyConverter;

import java.io.File;
import java.util.List;

/**
 * Created by Gaurav Dingolia on 19/01/16.
 */
public class Uploader
{
    /*****
     * RESPONSIBILITIES
     ****/
    // Should be able to upload any given file to the provided url or the default url.
    // Must only be concerned with upload mechanism
    // Should not care about which thread the code will be executed

    private static final String DEFAULT_UPLOAD_URL = "http://raven.kuikr.com/upload?source=mobileapp";

    private static final String BOUNDARY = "s2retfgsGSRFsERFGHfgdfgw734yhFHW567TYHSrf4yarg";

    private Uploader()
    {
    }

    public static <T> Response<T> uploadFile(String name, File file, ResponseBodyConverter<T> converter)
    {
        return uploadFile(DEFAULT_UPLOAD_URL, name, file, converter);
    }

    public static Response<String> uploadFile(String name, File file)
    {
        return uploadFile(DEFAULT_UPLOAD_URL, name, file);
    }

    public static <T> Response<T> uploadFile(String url, String name, File file, ResponseBodyConverter<T> converter)
    {
        HTTPRequest request = createHTTPRequest(url, name, file);
        return request.execute(converter);
    }

    public static Response<String> uploadFile(String url, String name, File file)
    {
        HTTPRequest request = createHTTPRequest(url, name, file);
        return request.execute();
    }

    private static HTTPRequest createHTTPRequest(String url, String name, File file)
    {
        if (file == null)
            throw new IllegalArgumentException("File cannot be null");

        if (TextUtils.isEmpty(url))
            throw new IllegalArgumentException("Not a valid url");


        FormPart part = new FormPart(null, name, file.getName(), FileRequestBody.newRequestBody(file));
        return createHTTPRequest(url, part);

    }

    private static HTTPRequest createHTTPRequest(String url, Context context, Uri uri, String name, String fileName)
    {
        FormPart part = new FormPart(null, name, fileName, new UriRequestBody(context, uri));
        return createHTTPRequest(url, part);
    }

    private static HTTPRequest createHTTPRequest(String url, FormPart part)
    {
        HTTPRequest.Builder builder = new HTTPRequest.Builder();
        builder.setUrl(url);
        builder.setMethod(Method.POST);
        builder.setRequestBody(createMultiPartRequestBody(part));
        return builder.build();
    }

    public static Response<String> uploadFile(Context context, Uri uri, String name, String fileName)
    {
        return uploadFile(context, DEFAULT_UPLOAD_URL, uri, name, fileName);
    }

    public static <T> Response<T> uploadFile(Context context, Uri uri, String name, String fileName,
                                             ResponseBodyConverter<T> converter)
    {
        return uploadFile(context, DEFAULT_UPLOAD_URL, uri, name, fileName, converter);
    }

    public static Response<String> uploadFile(Context context, String url, Uri uri, String name, String fileName)
    {
        HTTPRequest request = createHTTPRequest(url, context, uri, name, fileName);
        return request.execute();
    }

    public static <T> Response<T> uploadFile(Context context, String url, Uri uri, String name, String fileName,
                                             ResponseBodyConverter<T> converter)
    {
        HTTPRequest request = createHTTPRequest(url, context, uri, name, fileName);
        return request.execute(converter);
    }

    public static Response<String> uploadImage(String path)
    {
        return Uploader.uploadFile("image", new File(path));
    }

    public static Response<String> uploadImage(String url,String path)
    {
        return Uploader.uploadFile(url,"image", new File(path));
    }

    public static Response<String> uploadImage(File file)
    {
        return Uploader.uploadFile("image", file);
    }

    public static Response<String> uploadImage(String url,File file)
    {
        return Uploader.uploadFile(url,"image", file);
    }

    public static Response<String> uploadImage(Context context, Uri uri)
    {
        return Uploader.uploadFile(context, uri, "image", uri.getLastPathSegment());
    }

    public static Response<String> uploadImage(String url,Context context, Uri uri)
    {
        return Uploader.uploadFile(context,url, uri, "image", uri.getLastPathSegment());
    }

    public static <T> Response<T> uploadImage(String path, ResponseBodyConverter<T> converter)
    {
        return Uploader.uploadFile("image", new File(path), converter);
    }

    public static <T> Response<T> uploadImage(String url,String path, ResponseBodyConverter<T> converter)
    {
        return Uploader.uploadFile(url,"image", new File(path), converter);
    }

    public static <T> Response<T> uploadImage(File file, ResponseBodyConverter<T> converter)
    {
        return Uploader.uploadFile("image", file, converter);
    }

    public static <T> Response<T> uploadImage(String url,File file, ResponseBodyConverter<T> converter)
    {
        return Uploader.uploadFile(url,"image", file, converter);
    }

    public static <T> Response<T> uploadImage(Context context, Uri uri, ResponseBodyConverter<T> converter)
    {
        return Uploader.uploadFile(context, uri, "image", uri.getLastPathSegment(), converter);
    }

    public static <T> Response<T> uploadImage(String url,Context context, Uri uri, ResponseBodyConverter<T> converter)
    {
        return Uploader.uploadFile(context,url, uri, "image", uri.getLastPathSegment(), converter);
    }

    private static MultiPartRequestBody createMultiPartRequestBody(FormPart part)
    {
        MultiPartRequestBody.Builder builder = new MultiPartRequestBody.Builder
                (BOUNDARY).setContentType(MultiPartRequestBody
                .CONTENT_TYPE_MULTIPART_FORM_DATA);

        if (part != null)
        {
            builder.addFormData(part.headers, part.name, part.fileName, part.requestBody);
        }

        return builder.build();
    }


    private static MultiPartRequestBody createMultiPartRequestBody(List<FormPart> parts)
    {
        MultiPartRequestBody.Builder builder = new MultiPartRequestBody.Builder
                (BOUNDARY).setContentType(MultiPartRequestBody
                .CONTENT_TYPE_MULTIPART_FORM_DATA);

        if (parts != null)
        {
            for (FormPart part : parts)
            {
                builder.addFormData(part.headers, part.name, part.fileName, part.requestBody);
            }
        }

        return builder.build();
    }

    private static class FormPart
    {
        Headers headers;
        String name;
        String fileName;
        RequestBody requestBody;

        FormPart(Headers headers, String name, String fileName, RequestBody body)
        {
            this.headers = headers;
            this.name = name;
            this.fileName = fileName;
            this.requestBody = body;
        }
    }


}
