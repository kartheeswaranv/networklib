package com.quikr.android.api;

/**
 * HTTP headers internal to Quikr Application
 */
public class QuikrHttpHeaders
{
    /**
     * App id received during registration
     */
    public static final String QDP_X_QUIKR_APP_ID = "X-Quikr-App-Id";
    /**
     * Token Id received after token generation
     */
    public static final String QDP_X_QUIKR_TOKEN_ID = "X-Quikr-Token-Id";
    /**
     * Signature => Hmac-sha1("token", appId + email + date in yyyy-MM-dd format)
     */
    public static final String QDP_X_QUIKR_SIGNATURE_V2 = "X-Quikr-Signature-v2";

    /**
     *  Client signature to go with every QDP request
     */
    public static final String QDP_X_QUIKR_CLIENT_SIGNATURE="X-QUIKR-CLIENT-SIGNATURE";

    /**
     * Value for this header must be the app version
     */
    public static final String QDP_X_QUIKR_CLIENT_VERSION="X-QUIKR-CLIENT-VERSION";

    /**
     * User session
     */
    public static final String USER_SESSION ="UserSession";


    private QuikrHttpHeaders()
    {
        //prevent initialization
    }

}
