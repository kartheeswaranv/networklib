package com.quikr.android.api.services;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.quikr.android.api.helper.ImageUploader;
import com.quikr.android.api.helper.UploadException;
import com.quikr.android.api.helper.Uploader;
import com.quikr.android.api.models.UploadResponse;
import com.quikr.android.network.Response;
import com.quikr.android.network.converter.ResponseBodyConverter;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Gaurav Dingolia on 20/01/16.
 */
public class UploadService extends Service
{
    private static final String TAG = "UploadService";

    private static final int NOTIFICATION_ID = 19153453;

    private static final int CPU_COUNT = Runtime.getRuntime().availableProcessors();
    private static final int CORE_POOL_SIZE = CPU_COUNT + 1;
    private static final int MAXIMUM_POOL_SIZE = CPU_COUNT * 2 + 1;
    private static final int KEEP_ALIVE_TIME = 1;

    private static ThreadPoolExecutor sExecutor;
    private LinkedBlockingQueue<Runnable> mBlockingQueue;

    private IBinder mBinder;

    private AtomicInteger mPendingTaskCount = new AtomicInteger();

    private Handler mHandler =new Handler();

    @Override
    public void onCreate()
    {
        super.onCreate();
        mBlockingQueue = new LinkedBlockingQueue<>();
        sExecutor = new ThreadPoolExecutor(CORE_POOL_SIZE, MAXIMUM_POOL_SIZE, KEEP_ALIVE_TIME, TimeUnit.SECONDS,
                mBlockingQueue);

        mBinder = new UploadServiceBinder();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        return START_FLAG_REDELIVERY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent)
    {
        return mBinder;
    }

    private <T> void notifyClient(FutureTask<Response<T>> futureTask, final Callback<T> callback, Uri uri)
    {
        if (mPendingTaskCount.decrementAndGet() <= 0)
        {
            stopForeground(true);
            stopSelf();
        }

        if (futureTask.isCancelled() || callback == null)
        {
            Log.d(TAG, "Task canceled with pending task count: " + mPendingTaskCount.get());
            return;
        }

        Log.d(TAG, "Task completed with pending task count: " + mPendingTaskCount.get());

        UploadException exception = null;
        try
        {
            Response<T> response = futureTask.get();
            if (response == null)
            {
                exception = new UploadException("Failed to get response");

            }
            else if (response.getStatusCode() < 200 || response.getStatusCode() > 299)
            {
                exception = new UploadException(response);
            }

            if (exception == null)
            {
                final UploadResponse<T> uploadResponse = new UploadResponse<>();
                uploadResponse.uri = uri;
                uploadResponse.response = response;
                mHandler.post(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        callback.onUploadSuccess(uploadResponse);
                    }
                });

                return;
            }
        }
        catch (InterruptedException e)
        {
            exception = new UploadException("Failed to get response");
        }
        catch (ExecutionException e)
        {
            exception = new UploadException("Failed to get response");
        }

        exception.uri = uri;
        final UploadException finalException = exception;
        mHandler.post(new Runnable()
        {
            @Override
            public void run()
            {
                callback.onUploadFailed(finalException);
            }
        });

    }

    public interface Callback<T>
    {
        void onUploadSuccess(UploadResponse<T> response);

        void onUploadFailed(UploadException exception);

    }

    public class UploadServiceBinder extends Binder
    {
        public <T> Future<Response<T>> uploadImage(final Uri uri, Callback<T> callback, final ResponseBodyConverter<T>
                converter)
        {
            Callable<Response<T>> callable = new Callable<Response<T>>()
            {
                @Override
                public Response<T> call() throws Exception
                {
                    return Uploader.uploadImage(getApplicationContext(), uri, converter);
                }
            };

            return submitCall(callable, callback, uri);
        }

        public <T> Future<Response<T>> uploadImage(final String url,final Uri uri, Callback<T> callback, final
        ResponseBodyConverter<T> converter)
        {
            Callable<Response<T>> callable = new Callable<Response<T>>()
            {
                @Override
                public Response<T> call() throws Exception
                {
                    return Uploader.uploadImage(url,getApplicationContext(), uri, converter);
                }
            };

            return submitCall(callable, callback, uri);
        }

        public Future<Response<String>> uploadImage(final Uri uri, final Callback<String> callback)
        {
            Callable<Response<String>> callable = new Callable<Response<String>>()
            {
                @Override
                public Response<String> call() throws Exception
                {
                    return Uploader.uploadImage(getApplicationContext(), uri);
                }
            };

            return submitCall(callable, callback, uri);
        }

        public Future<Response<String>> uploadImage(final String url, final Uri uri, final Callback<String> callback)
        {
            Callable<Response<String>> callable = new Callable<Response<String>>()
            {
                @Override
                public Response<String> call() throws Exception
                {
                    return Uploader.uploadImage(url,getApplicationContext(), uri);
                }
            };

            return submitCall(callable, callback, uri);
        }


        private <T> Future<Response<T>> submitCall(Callable<Response<T>> call, final Callback<T> callback,
                                                   final Uri uri)
        {
            FutureTask<Response<T>> futureTask = new FutureTask<Response<T>>(call)
            {
                @Override
                protected void done()
                {
                    super.done();
                    notifyClient(this, callback, uri);
                }
            };

            startForeground(NOTIFICATION_ID, getNotification());
            mPendingTaskCount.incrementAndGet();
            sExecutor.execute(futureTask);

            return futureTask;
        }

        private Notification getNotification()
        {
            NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext())
                    .setContentTitle("Uploading content")
                    .setContentText("Your content is being uploaded").setProgress(100, 0, true)
                    .setSmallIcon(android.R.drawable.stat_sys_upload);

            Intent intent = new Intent(getApplicationContext(), ImageUploader.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

            PendingIntent pendingIntent = PendingIntent
                    .getActivity(getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            builder.setContentIntent(pendingIntent);
            return builder.build();

        }
    }

}
