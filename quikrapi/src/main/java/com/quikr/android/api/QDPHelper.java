package com.quikr.android.api;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Log;

import com.quikr.android.api.models.QDPTokenRequestBody;
import com.quikr.android.api.models.QDPTokenResponseBody;
import com.quikr.android.network.HttpHeaders;
import com.quikr.android.network.Method;
import com.quikr.android.network.Request;
import com.quikr.android.network.Response;
import com.quikr.android.network.converter.GsonRequestBodyConverter;
import com.quikr.android.network.converter.GsonResponseBodyConverter;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import static android.R.attr.handle;

/**
 *
 */
class QDPHelper
{
    protected static final int TOKEN_GEN_TIMEOUT = 30;
    private static final String TAG = "QDPHelper";

    private static final String STORAGE_FILE = "QDP";

    private static final String STORAGE_KEY_QDP_TOKEN = "QDP_token";
    private static final String STORAGE_KEY_QDP_TOKEN_ID = "QDP_token_id";
    private static final String QDP_TOKEN_EXPIRY_MILLIS = "QDP_TOKEN_EXPIRY_MILLIS";
    public static final String KEY_QDP_TOKEN_ADDED_TIMESTAMP = "QDP_TOKEN_ADDED_TIMESTAMP";

    private static final QDPMetaData sQDPMetadata = QuikrNetwork.getQuikrContext().getQDPMetaData();

    private static final String X_QUIKR_CLIENT_SIGNATURE = "zXcv80386Mdp1hs0q7o0p9uiLZV37TdF";
    private static volatile QDPHelper mInstance;
    private Set<String> mQDPHeaders;
    private SimpleDateFormat mDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

    private Storage mStorage;
    /**
     * 0 if token was created in previous launch, else timestamp when token was stored
     * Used to prevent an old request's auth error leading to invalidation of a newer token
     */
    protected volatile long tokenTimestamp = 0;
    private volatile boolean mGeneratingToken;

    {
        Set<String> set = new HashSet<>();
        set.add(QuikrHttpHeaders.QDP_X_QUIKR_APP_ID);
        set.add(QuikrHttpHeaders.QDP_X_QUIKR_TOKEN_ID);
        set.add(QuikrHttpHeaders.QDP_X_QUIKR_SIGNATURE_V2);
        set.add(QuikrHttpHeaders.QDP_X_QUIKR_CLIENT_SIGNATURE);
        set.add(QuikrHttpHeaders.QDP_X_QUIKR_CLIENT_VERSION);

        mQDPHeaders = Collections.unmodifiableSet(set);
    }

    private QDPHelper(Context context)
    {
        mStorage = new SharedPreferenceStorage(context, STORAGE_FILE);
        tokenExecutor.allowCoreThreadTimeOut(true);
    }

    public boolean isTokenValid()
    {
        //Token expiration logic was not working as it was || instead of &&
        //Removed the hard coded logic which can have issues like executing just before midnight
        return mStorage.retrieve(STORAGE_KEY_QDP_TOKEN) != null;
    }

    /*public boolean isTokenExpired()
    {
        String millis = mStorage.retrieve(QDP_TOKEN_EXPIRY_MILLIS);

        return millis == null || Long.parseLong(millis) < System
                .currentTimeMillis();
    }*/

    void clearToken()
    {
        mStorage.remove(STORAGE_KEY_QDP_TOKEN);
        mStorage.remove(STORAGE_KEY_QDP_TOKEN_ID);
        mStorage.remove(QDP_TOKEN_EXPIRY_MILLIS);

    }

    public void refreshTokenIfNeeded()
    {
        if (sQDPMetadata == null || sQDPMetadata.getTokenGenerationUrl() == null ||isTokenValid()) return;

        generateNewToken();
    }

    public boolean isTokenProcessing()
    {
        return mGeneratingToken;
    }

    /**
     * Single threaded background executor for token generation
     */
    protected ThreadPoolExecutor tokenExecutor = new ThreadPoolExecutor(1, // core size
        1, // max size
        1, // idle timeout
        TimeUnit.MINUTES,
        new ArrayBlockingQueue<Runnable>(1),
        new ThreadPoolExecutor.DiscardPolicy());

    public synchronized void generateNewToken()
    {
        if (mGeneratingToken)
            return;

        mGeneratingToken = true;
        clearToken();
        //Only the first thread executing the following statement will reset the latch. Others will do nothing.
        QuikrNetwork.getDispatcher().resetLatchIfNeeded();

        final Request request = new Request.Builder().setUrl(sQDPMetadata.getTokenGenerationUrl())
            .setMethod(Method.POST)
            .addHeader(HttpHeaders
                .RequestHeaders.CONTENT_TYPE, "application/json")
            .setBody(getNewQDPTokenRequestBody(), new GsonRequestBodyConverter
                <QDPTokenRequestBody>()).setPriority(Request.Priority.IMMEDIATE).build();

        tokenExecutor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    Log.d(TAG, "Starting generating token ");
                    final Response<QDPTokenResponseBody> response = QuikrNetwork.getNetworkManager()
                        .executeSynchronously(
                            request,
                            new GsonResponseBodyConverter<QDPTokenResponseBody>(
                                QDPTokenResponseBody.class));

                    final boolean isError = response == null || response.getBody() == null
                        || "true".equalsIgnoreCase(response.getBody().error);

                    if (isError) {
                        onError(new RuntimeException());
                    } else {
                        onSuccess(response);
                    }
                } catch (final Exception e) {
                    onError(e);
                }
            }

            protected void onError (Exception e) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        Log.d(TAG, "Error in token generation");
                        QuikrNetwork.getDispatcher().clearQueuedRequests();
                        mGeneratingToken = false;
                        QuikrNetwork.getDispatcher().proceed();
                    }
                });
            }

            protected void onSuccess (final Response<QDPTokenResponseBody> response) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        Log.d(TAG, "Token generated");
                        storeTokenResponse(response.getBody());
                        mGeneratingToken = false;
                        QuikrNetwork.getDispatcher().proceed();
                    }
                });
            }
        });
    }

    public long getTokenTimestamp() {
        return tokenTimestamp;
    }

    private void storeTokenResponse(QDPTokenResponseBody response)
    {
        mStorage.store(STORAGE_KEY_QDP_TOKEN, response.token);
        mStorage.store(STORAGE_KEY_QDP_TOKEN_ID, String.valueOf(response.tokenId));
        tokenTimestamp = System.currentTimeMillis();

        /*Calendar calendar = Calendar.getInstance();
        long currentMillis = calendar.getTimeInMillis();
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        mStorage.store(QDP_TOKEN_EXPIRY_MILLIS, String.valueOf(calendar.getTimeInMillis() - currentMillis));*/
    }

    private QDPTokenRequestBody getNewQDPTokenRequestBody()
    {
        QDPTokenRequestBody request = new QDPTokenRequestBody();
        request.appId = QuikrNetwork.getQuikrContext().getQDPMetaData().getAppId();
        request.signature = QDPUtils.generateHMAC(sQDPMetadata.getAppSecret(),
                sQDPMetadata.getAppDeveloperEmail() + sQDPMetadata.getAppId() + mDateFormat.format
                        (Calendar.getInstance().getTime()));
        return request;

    }

    public static Request doQDPRelatedStuff(Request request)
    {
        QDPHelper helper = getInstance();

        request.putHeader(QuikrHttpHeaders.QDP_X_QUIKR_APP_ID, sQDPMetadata.getAppId());
        request.putHeader(QuikrHttpHeaders.QDP_X_QUIKR_CLIENT_SIGNATURE, X_QUIKR_CLIENT_SIGNATURE);
        request.putHeader(QuikrHttpHeaders.QDP_X_QUIKR_CLIENT_VERSION,
                QuikrNetwork.getQuikrContext().getQDPClientVersion());

        String tokenId = helper.mStorage.retrieve(STORAGE_KEY_QDP_TOKEN_ID);
        request.putHeader(QuikrHttpHeaders.QDP_X_QUIKR_TOKEN_ID, TextUtils.isEmpty(tokenId) ? "" : tokenId);

        String message = sQDPMetadata.getAppId() + sQDPMetadata.getAppDeveloperEmail()
                + helper.mDateFormat.format(Calendar.getInstance().getTime());
        String key = helper.mStorage.retrieve(STORAGE_KEY_QDP_TOKEN);

        String signature = TextUtils.isEmpty(key) ? "" : QDPUtils.generateHMAC(key, message);
        request.putHeader(QuikrHttpHeaders.QDP_X_QUIKR_SIGNATURE_V2, signature);
        request.markers.put(KEY_QDP_TOKEN_ADDED_TIMESTAMP, System.currentTimeMillis());

        return request;
    }

    public static QDPHelper getInstance()
    {
        if (mInstance == null)
        {
            synchronized (QDPHelper.class)
            {
                if (mInstance == null)
                {
                    mInstance = new QDPHelper(QuikrNetwork.getQuikrContext().getApplicationContext());
                }
            }
        }
        return mInstance;
    }

    static boolean isQDPRequest(Request request)
    {
        if (request.getTag() instanceof QuikrRequest.TagWrapper)
        {
            QuikrRequest.TagWrapper wrapper = (QuikrRequest.TagWrapper) request.getTag();
            return ((QuikrRequest) wrapper.getWrapperTag()).isQDPRequest();
        }

        return false;
    }

    private static Set<String> getQDPRequestHeaders()
    {
        return getInstance().mQDPHeaders;
    }



}
