package com.quikr.android.api;

import android.content.Context;

import java.util.Map;
import java.util.Set;

/**
 * Created by Gaurav Dingolia on 11/01/16.
 */
public interface QuikrContext
{
    String PARAM_URL = "url";
    String PARAM_CONTENT_TYPE = "contentType";

    /**
     * Gets Application Context.
     * @return Application context
     */
    Context getApplicationContext();

    /**
     * Basic headers that can be appended to any request before actually executing it.
     * <p><b>Note: </b>The basic headers will not override any header, if present.</p>
     * @param isQDP is QPD Request
     * @return
     */
    Map<String,String> getBasicHeaders(boolean isQDP);

    /**
     * Basic params indicates params that needs to be appended with each request (if mentioned).
     * @return params map to be appended to each request url or {@code null} if not required.
     */
    Map<String,String> getBasicParams(boolean isQDP);

    /**
     * Must return client version needed for QDP requests
     * @return QDP client version
     */
    String getQDPClientVersion();

    /**
     * A {@link QDPMetaData} which will be used for all QDP requests. If {@code null}, the default values will be used.
     * @return
     */
    QDPMetaData getQDPMetaData();

    Map<String,Object> getContextProperties();

    /**
     * Set of domain names that support encryption in the app
     */
    Set<String> getEncrytionSupportedUrls();

    boolean isCompressionRequiredForParams(Map<String, String> params);

    boolean isEncryptionEnabled();

    interface Properties{
        String VOLLEY_CONTEXT = "volleyContext";
    }

    Map<String, String> getUTMParams();

}
