package com.quikr.android.api.helper;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.IBinder;
import android.text.TextUtils;
import android.util.Log;

import com.quikr.android.api.QuikrNetwork;
import com.quikr.android.api.models.UploadResponse;
import com.quikr.android.api.services.UploadService;
import com.quikr.android.network.converter.ResponseBodyConverter;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Gaurav Dingolia on 19/01/16.
 */
public final class ImageUploader
{
    private static final String TAG = "ImageUploader";

    private ServiceConnection mServiceConnection = new UploadServiceConnection();
    private LinkedList<UploadItem> mQueue = new LinkedList<>();
    private UploadService.UploadServiceBinder mBinder;
    private List<UploadTask> mRemainingTasks = new ArrayList<>();

    private AtomicInteger mCount = new AtomicInteger();
    private String mBaseUrl;

    private boolean mConnected;
    private boolean mConnecting;

    private Context mContext;

    public ImageUploader()
    {
        mContext = QuikrNetwork.getQuikrContext().getApplicationContext();
    }

    public String getBaseUrl()
    {
        return mBaseUrl;
    }

    public void setBaseUrl(String url)
    {
        mBaseUrl = url;
    }

    public UploadTask uploadImage(Uri uri, Callback<String> callback)
    {
        return uploadImageInternal(callback, null, uri);
    }

    public UploadTask uploadImages(Uri[] uris, Callback<String> callback)
    {
        return uploadImageInternal(callback, null, uris);
    }

    public <T> UploadTask uploadImage(Uri uri, Callback<T> callback, ResponseBodyConverter<T> converter)
    {
        return uploadImageInternal(callback, converter, uri);
    }

    public <T> UploadTask uploadImages(Uri[] uris, Callback<T> callback, ResponseBodyConverter<T>
            converter)
    {
        return uploadImageInternal(callback, converter, uris);
    }

    public UploadTask uploadImages(String url, Uri[] uris, Callback<String> callback)
    {
        return uploadImageInternal(url, callback, null, uris);
    }

    public <T> UploadTask uploadImage(String url, Uri uri, Callback<T> callback, ResponseBodyConverter<T> converter)
    {
        return uploadImageInternal(url, callback, converter, uri);
    }

    public <T> UploadTask uploadImages(String url, Uri[] uris, Callback<T> callback, ResponseBodyConverter<T>
            converter)
    {
        return uploadImageInternal(url, callback, converter, uris);
    }

    private <T> UploadTask uploadImageInternal(Callback<T> callback, ResponseBodyConverter<T> converter,
                                               Uri... uri)
    {
        return uploadImageInternal(null, callback, converter, uri);
    }

    private <T> UploadTask uploadImageInternal(String url, Callback<T> callback, ResponseBodyConverter<T> converter,
                                               Uri... uri)
    {
        UploadTask task = new ImageUploadTask.Builder(mRemainingTasks).add(uri).build();
        mRemainingTasks.add(task);
        addToQueue(new UploadItem(url, task, new CallbackWrapper(callback, task), converter));
        return task;
    }

    private void addToQueue(UploadItem item)
    {
        if (mQueue.offer(item))
            mCount.incrementAndGet();

        if (mConnected)
            executePendingTasks();
        else
            connect();
    }

    private void connect()
    {
        if (mConnected || mConnecting)
            return;

        mConnecting = true;

        Intent intent = new Intent(mContext, UploadService.class);
        mContext.bindService(intent, mServiceConnection, Context.BIND_AUTO_CREATE);
    }

    private void disconnect()
    {
        if (mConnected)
        {
            mContext.unbindService(mServiceConnection);
            mConnected = false;
            Log.d(TAG, "Disconnected");
        }
        mBinder = null;
    }

    public void destroy()
    {
        cleanUp();
        disconnect();
        mServiceConnection = null;
        mContext = null;
    }

    private void cleanUp()
    {
        // This is required as the cancel() method is modifying list
        List<UploadTask> list = new ArrayList<>(mRemainingTasks);

        for (UploadTask task : list)
        {
            task.cancel(true);
        }

        mRemainingTasks.clear();
        mQueue.clear();
    }

    private void executePendingTasks()
    {
        UploadItem item;
        while ((item = mQueue.poll()) != null)
        {
            if (item.task.isCanceled())
                continue;

            Uri[] uris = item.task.getUris();

            for (int i = 0; i < uris.length; i++)
            {
                Future future;
                String url = TextUtils.isEmpty(item.url) ? mBaseUrl : item.url;

                if (item.converter == null)
                {
                    if (TextUtils.isEmpty(url))
                        future = mBinder.uploadImage(uris[i], item.callback);
                    else
                        future = mBinder.uploadImage(url, uris[i], item.callback);

                }
                else
                {
                    if (TextUtils.isEmpty(url))
                        future = mBinder.uploadImage(uris[i], item.callback, item.converter);
                    else
                        future = mBinder.uploadImage(url, uris[i], item.callback, item.converter);
                }

                item.task.setFuture(i, future);
            }
        }
    }

    public interface Callback<T> extends UploadService.Callback<T>
    {
        void onUploadComplete(boolean success);
    }

    private static class ImageUploadTask extends UploadTask
    {
        private WeakReference<List<UploadTask>> mListReference;

        ImageUploadTask(Builder builder)
        {
            super(builder);
            mListReference = builder.list == null ? null : new WeakReference<>(builder.list);
        }

        @Override
        protected void onCanceled()
        {
            super.onCanceled();

            List<UploadTask> list = mListReference == null ? null : mListReference.get();
            if (list != null)
            {
                boolean isRemoved = list.remove(this);
                Log.d(TAG, "onCanceled: Removed - " + isRemoved);
            }
        }

        private static final class Builder extends UploadTask.Builder
        {
            private List<UploadTask> list;

            public Builder(List<UploadTask> list)
            {
                this.list = list;
            }

            @Override
            public UploadTask build()
            {
                return new ImageUploadTask(this);
            }
        }
    }

    private class UploadItem
    {
        private String url;
        private UploadTask task;
        private UploadService.Callback callback;
        private ResponseBodyConverter converter;

        UploadItem(UploadTask task, UploadService.Callback callback, ResponseBodyConverter converter)
        {
            this(null, task, callback, converter);
        }

        UploadItem(String url, UploadTask task, UploadService.Callback callback, ResponseBodyConverter converter)
        {
            this.url = url;
            this.task = task;
            this.callback = callback;
            this.converter = converter;
        }

    }

    private class UploadServiceConnection implements ServiceConnection
    {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service)
        {
            mBinder = (UploadService.UploadServiceBinder) service;

            mConnecting = false;
            mConnected = true;

            executePendingTasks();
        }

        @Override
        public void onServiceDisconnected(ComponentName name)
        {
            mConnecting = false;
            mConnected = false;
        }
    }

    private class CallbackWrapper<T> implements UploadService.Callback<T>
    {
        private Callback<T> mCallback;
        private UploadTask mTask;
        private int mCompleted;
        private boolean mSuccess = true;

        private CallbackWrapper(Callback<T> callback, UploadTask task)
        {
            mCallback = callback;
            mTask = task;
        }

        @Override
        public void onUploadSuccess(UploadResponse<T> response)
        {
            if (mCallback != null)
            {
                mCallback.onUploadSuccess(response);
            }

            update();
        }

        @Override
        public void onUploadFailed(UploadException exception)
        {
            if (mCallback != null)
                mCallback.onUploadFailed(exception);

            mSuccess = false;
            update();
        }

        private void update()
        {
            if (++mCompleted == mTask.getTaskCount())
            {
                // We are done
                if (mCallback != null)
                    mCallback.onUploadComplete(mSuccess);

                if (mCount.decrementAndGet() == 0)
                    disconnect();

                cleanUp();
            }
        }

        private void cleanUp()
        {
            mRemainingTasks.remove(mTask);
            mTask = null;
            mCallback = null;
        }
    }

}
