package com.quikr.android.api;

/**
 * Created by Gaurav Dingolia on 30/03/16.
 */
public final class QDPMetaData
{
    private String mBaseUrl;
    private String mTokenGenerationUrl;
    private String mAppSecret;
    private String mAppId;
    private String mAppDeveloperEmail;

    public String getAppSecret()
    {
        return mAppSecret;
    }

    public void setAppSecret(String appSecret)
    {
        this.mAppSecret = appSecret;
    }

    public String getAppId()
    {
        return mAppId;
    }

    public void setAppId(String appId)
    {
        this.mAppId = appId;
    }

    public String getAppDeveloperEmail()
    {
        return mAppDeveloperEmail;
    }

    public void setAppDeveloperEmail(String appDeveloperEmail)
    {
        this.mAppDeveloperEmail = appDeveloperEmail;
    }

    public String getBaseUrl()
    {
        return mBaseUrl;
    }

    public void setBaseUrl(String mBaseUrl)
    {
        this.mBaseUrl = mBaseUrl;
    }

    public String getTokenGenerationUrl()
    {
        return mTokenGenerationUrl;
    }

    public void setTokenGenerationUrl(String mTokenGenerationUrl)
    {
        this.mTokenGenerationUrl = mTokenGenerationUrl;
    }

}
