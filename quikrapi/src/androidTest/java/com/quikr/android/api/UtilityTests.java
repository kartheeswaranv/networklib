package com.quikr.android.api;

import android.net.Uri;
import android.test.AndroidTestCase;
import android.test.suitebuilder.annotation.SmallTest;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Gaurav Dingolia on 08/01/16.
 */
public class UtilityTests extends AndroidTestCase
{
    private static final String TAG = "UtilityTests";

    public void testValidUrls()
    {
        List<String> urls = new ArrayList<>();
        urls.add("http://www.quikr.com");
        urls.add("www.test.com");

        for (String url : urls)
        {
            String utilUrl = Utils.appendParams(url, null,false);
            Log.d(TAG, "Util url: " + url);
            assertEquals(url, utilUrl);
        }
    }

    public void testAddingParams()
    {
        String url = "http://www.quikr.com";
        Map<String, String> params = new HashMap<>();
        params.put("param1", "value1");
        params.put("param2", "value2");

        String newUrl = Utils.appendParams(url, params,false);
        Log.d(TAG, "Url with param: " + newUrl);

        assertEquals(newUrl, url + "?param1=value1&param2=value2");

    }

    @SmallTest
    public void testUriOverride()
    {
        String url = "http://www.quikr.com/api?key1=value1&key2=value2";
        Map<String,String> params = new HashMap<>();
        params.put("key1","value10");
        params.put("key3","value3");

        assertEquals(Uri.parse("http://www.quikr.com/api?key1=value10&key2=value2&key3=value3"),Uri.parse(Utils
                .appendParams(url,params, true)));
//        assertTrue(Uri.parse("http://www.quikr.com/api?key1=value1&key2=value2&key3=value3").equals(Uri.parse(Utils
//                .appendParams(url,params, false))));
    }


}
