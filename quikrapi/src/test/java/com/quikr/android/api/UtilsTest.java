package com.quikr.android.api;

import junit.framework.TestCase;

import org.junit.Test;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by Gaurav Dingolia on 10/01/16.
 */
public class UtilsTest extends TestCase
{
    private static final String SECRET_KEY = "1b4a2e8de9f42b3d03cf19d4e8598275769ff2c6";

    @Test
    public void testHMAC()
    {
        Map<String, String> messages = new HashMap<>();
        messages.put("This is a unit test.", "26a650011ffc5f120cac64ee1df12bb16d6c3fe0");
        messages.put("If this test runs fine, then the algorithm is working :)",
                "b1ace90963a0ee2b222509d2ba935b9a3fe4ea40");

        for (Iterator<Map.Entry<String, String>> iterator = messages.entrySet().iterator(); iterator.hasNext(); )
        {
            Map.Entry<String, String> entry = iterator.next();
            assertEquals(Utils.generateHMAC_SHA1(SECRET_KEY, entry.getKey()), entry.getValue());
        }


    }

}
