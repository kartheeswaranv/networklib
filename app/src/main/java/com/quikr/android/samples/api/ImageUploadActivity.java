package com.quikr.android.samples.api;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.quikr.android.api.helper.ImageUploader;
import com.quikr.android.api.helper.UploadException;
import com.quikr.android.api.helper.UploadTask;
import com.quikr.android.api.helper.Uploader;
import com.quikr.android.network.Response;
import com.quikr.android.network.converter.GsonResponseBodyConverter;
import com.quikr.android.samples.api.models.UploadResponse;

public class ImageUploadActivity extends AppCompatActivity
{
    private static final String TAG = "ImageUploadActivity";

    private static final int REQUEST_CODE_FILE = 100;

    FloatingActionButton mFab;
    private AppCompatActivity mInstance;
    private TextView mTextView;

    private ImageUploader mUploader = new ImageUploader();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        mInstance = this;
        setContentView(R.layout.activity_image_upload);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mTextView = (TextView) findViewById(R.id.textview);

        mFab = (FloatingActionButton) findViewById(R.id.fab);
        mFab.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                launchFilesIntent();
            }
        });
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        mUploader.destroy();
        mInstance = null;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_FILE && resultCode == RESULT_OK)
        {
            showSnackBar("Uploading Image. Please wait");
            final Uri uri = data.getData();
            Log.d(TAG, "onActivityResult,Uri: " + uri.toString());

            doExperiment(uri);
        }
        else if (requestCode == REQUEST_CODE_FILE)
        {
            showSnackBar("Failed to retrieve image");
        }
    }

    private void launchFilesIntent()
    {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");

        String title = "Choose file via";
        Intent chooser = Intent.createChooser(intent, title);

        if (intent.resolveActivity(getPackageManager()) != null)
        {
            startActivityForResult(chooser, REQUEST_CODE_FILE);

        }
    }

    private void doExperiment(Uri uri)
    {
        Uri[] uris = new Uri[1];
        for (int i = 0; i < uris.length; i++)
        {
            uris[i] = uri;
        }
        final UploadTask task = uploadImage(uris);


//        new Handler().postDelayed(new Runnable()
//        {
//            @Override
//            public void run()
//            {
//                task.cancel(true);
//            }
//        }, 100);
    }

    private UploadTask uploadImage(Uri... uri)
    {
        return mUploader.uploadImages("http://www.dummyurl.com/upload",uri, new ImageUploader.Callback<UploadResponse>()
        {
            @Override
            public void onUploadComplete(boolean success)
            {
                Log.d(TAG, "onUploadComplete: Success - " + success);
                mTextView.setText("Upload finished");
            }

            @Override
            public void onUploadSuccess(com.quikr.android.api.models.UploadResponse<UploadResponse> response)
            {
                Log.d(TAG, "onUploadSuccess: " + response.response.getBody().toString());
                showSnackBar("Upload successful");

            }

            @Override
            public void onUploadFailed(UploadException exception)
            {
                Log.d(TAG, "onUploadFailed: " + exception.uri);
                showSnackBar("Upload failed");
            }
        }, new GsonResponseBodyConverter<UploadResponse>(UploadResponse.class));
    }

    private void showSnackBar(String message)
    {
        if (mInstance == null)
            return;

        Snackbar.make(mFab, message, Snackbar.LENGTH_LONG).show();
    }

    private class ImageUploadAsyncTask extends AsyncTask<Uri, Integer, Boolean>
    {

        @Override
        protected Boolean doInBackground(Uri... params)
        {
            if (params.length == 0)
                return false;

            Uri uri = params[0];

            Response<UploadResponse> response = Uploader.uploadImage(ImageUploadActivity.this, uri, new
                    GsonResponseBodyConverter<UploadResponse>(UploadResponse.class));

            Log.d(TAG, "Response Body: " + (response.getBody() == null ? null : response.getBody().toString()));

            if (response != null && (response.getStatusCode() >= 200 && response.getStatusCode() < 299))
                return true;

            return false;
        }

        @Override
        protected void onPostExecute(Boolean success)
        {
            if (mInstance == null)
                return;

            super.onPostExecute(success);
            showSnackBar(success ? "Upload successful" : "Upload failed");
        }

    }


}
