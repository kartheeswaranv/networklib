package com.quikr.android.samples.api.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Gaurav Dingolia on 25/01/16.
 */
public class UploadResponse
{
    @SerializedName("status")
    public String status;

    @SerializedName("filename")
    public String name;

    @SerializedName("filesize")
    public String size;

    @SerializedName("fileurl")
    public String url;

    @Override
    public String toString()
    {
        return url;
    }
}
