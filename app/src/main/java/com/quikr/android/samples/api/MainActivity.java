package com.quikr.android.samples.api;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.quikr.android.api.QDPMetaData;
import com.quikr.android.api.QuikrContext;
import com.quikr.android.api.QuikrNetwork;
import com.quikr.android.api.QuikrRequest;
import com.quikr.android.network.Callback;
import com.quikr.android.network.Method;
import com.quikr.android.network.NetworkException;
import com.quikr.android.network.Request;
import com.quikr.android.network.Response;
import com.quikr.android.network.converter.ToStringResponseBodyConverter;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class MainActivity extends AppCompatActivity
{
    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        QuikrNetwork.initialize(new MyContext());

        makeEncryptedRequest();
    }

    private void makeEncryptedRequest() {
        Map<String, String> headers = new HashMap<>();
        headers.put("X-Quikr-Signature-v2", "e7b7cddb91aadf91119981c79fdd75d04e02e918");
        headers.put("X-Quikr-Token-Id", "175966603");
        headers.put("X-Quikr-App-Id", "863");
        headers.put("User-Agent", "QuikrConsumer");
        headers.put("X-QUIKR-CLIENT", "Android.9_01");
        headers.put("X-QUIKR-CLIENT-DEMAIL", "qadyk2a_k0baqdev1960s@gmail.com");
        headers.put("X-QUIKR-CLIENT-SIGNATURE", "zXcv80386Mdp1hs0q7o0p9uiLZV37TdF");
        headers.put("X-QUIKR-CLIENT-VERSION", "9.01");
        headers.put("X-QUIKR-CLIENT-LANG-CODE", "en");

        QuikrRequest r = new QuikrRequest.Builder()
                .setMethod(Method.GET)
                .setUrl("https://api.quikr.com/mqdp/v1/gettrends?city=23&opf=json&demail=qadyk2a_k0baqdev1960s@gmail.com")
                .setContentType("application/json")
                .addHeaders(headers)
                .build();
        r.execute(new Callback<String>() {
            @Override
            public void onSuccess(Response<String> response) {
                Log.i("qqq", "yeah == " + response.getBody());
            }

            @Override
            public void onError(NetworkException e) {
                Log.i("qqq", "fayl" + e.getMessage());
            }
        }, new ToStringResponseBodyConverter());
    }

    private void makeWrapperRequest()
    {

        Request request = new Request.Builder().setUrl("http://www.testurl.com/test12").addHeader("Header1", "value1")
                .build();
        QuikrNetwork.getNetworkManager().execute(request);
    }

    private void makeQuikrRequest()
    {
        QuikrRequest request = new QuikrRequest.Builder().setUrl("http://www.testurl.com/test12").addHeader
                ("Header1", "Value1").appendBasicHeaders
                (true).appendBasicParams(true).setMethod(Method.GET).build();

        request.execute();
    }


    private class MyContext implements QuikrContext
    {
        Map<String, String> headers = new HashMap<>();

        {
            headers.put("HeaderBasic1", "ValueBasic1");
        }

        @Override
        public Context getApplicationContext()
        {
            return MainActivity.this.getApplicationContext();
        }

        @Override
        public Map<String, String> getBasicHeaders(boolean isQDP)
        {
            return headers;
        }

        @Override
        public Map<String, String> getBasicParams(boolean isQDP)
        {
            return null;
        }

        @Override
        public String getQDPClientVersion()
        {
            return "8.5";
        }

        @Override
        public QDPMetaData getQDPMetaData()
        {
            QDPMetaData m = new QDPMetaData();
            m.setAppSecret("9342dd76250285bb6bf5d5e8a1ba2bb7");
            return m;
        }

        @Override
        public Map<String, Object> getContextProperties() {
            return new HashMap<>();
        }

        @Override
        public Set<String> getEncrytionSupportedUrls() {
            Set<String> s = new HashSet<>();
            s.add("api.quikr.com");
            s.add("192.168.124.53:8500");
            return s;
        }

        @Override
        public boolean isCompressionRequiredForParams(Map<String, String> params) {
            return false;
        }

        @Override
        public boolean isEncryptionEnabled() {
            return true;
        }

        @Override
        public Map<String, String> getUTMParams() {
            return null;
        }
    }


}
